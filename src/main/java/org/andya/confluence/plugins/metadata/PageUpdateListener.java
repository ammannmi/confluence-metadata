package org.andya.confluence.plugins.metadata;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.page.PageEvent;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.event.events.content.page.PageTrashedEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.confluence.event.events.plugin.PluginEvent;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;

public class PageUpdateListener implements DisposableBean {
    private static final Logger log = Logger.getLogger(PageUpdateListener.class);

    private static final String[] PLUGINS_USING_METADATAMODEL = {"com.comalatech.adhoccanvas",
            "com.comalatech.checklists","com.comalatech.workflow"};
    public static final String UNSAFE_KEY = "org.andya.confluence.plugins.metadata.unsafe";

    private ContentPropertyManager contentPropertyManager;
    private BandanaManager bandanaManager;
    private EventPublisher eventPublisher;
    private PluginAccessor pluginAccessor;
    private Renderer viewRenderer;
    private boolean canClearMetadata = false;

    public PageUpdateListener(ContentPropertyManager contentPropertyManager, EventPublisher eventPublisher, PluginAccessor pluginAccessor, Renderer viewRenderer, BandanaManager bandanaManager) {
        this.contentPropertyManager = contentPropertyManager;
        this.eventPublisher = eventPublisher;
        this.pluginAccessor = pluginAccessor;
        this.viewRenderer = viewRenderer;
        this.bandanaManager = bandanaManager;
        handlePluginEvent(null);
        if (canClearMetadata) {
            eventPublisher.register(this);
        }
    }

    @EventListener
    /**
     * Will determine if the clearing of metadata is safe, by checking if the plugins
     * using metadata model are not installed
     */
    public void handlePluginEvent(PluginEvent event) {
        canClearMetadata = isSafeToClearMetadata();
    }

    private boolean isSafeToClearMetadata() {
        Boolean unsafe = (Boolean) bandanaManager.getValue(new ConfluenceBandanaContext(), UNSAFE_KEY);
        if (unsafe != null && unsafe) {
            log.warn("marked unsafe already. If you want to remove metadata if metadata macros are " +
                    "removed from page's content, then remove bandana value " +
                    "'org.andya.confluence.plugins.metadata.unsafe'");
            return false;
        }
        for (String pluginkey : PLUGINS_USING_METADATAMODEL) {
            if (pluginAccessor.getEnabledPlugin(pluginkey) != null) {
                log.warn("Not safe to clear metadata on page updates");
                bandanaManager.setValue(new ConfluenceBandanaContext(),UNSAFE_KEY,true);
                return false;
            }
        }
        log.warn("Metadata values will be remove if metadata macros are removed from page's content");
        return true;
    }

    @EventListener
    public void handleRemoveEvent(PageRemoveEvent event) {
        if (canClearMetadata) {
            MetadataUtils.clearMetadata(contentPropertyManager,event.getContent());
        }
    }

    @EventListener
    public void handleTrashedEvent(PageTrashedEvent event) {
        if (canClearMetadata) {
            MetadataUtils.clearMetadata(contentPropertyManager,event.getContent());
        }
    }

    @EventListener
    public void handleUpdatedEvent(PageUpdateEvent event) {
        clearAndRerender(event);
    }

    @EventListener
    public void handleRestoreEvent(PageRestoreEvent event) {
        clearAndRerender(event);
    }

    // DO NOT make EventListener
    private void clearAndRerender(PageEvent event) {
        if (canClearMetadata) {
            MetadataUtils.clearMetadata(contentPropertyManager, event.getContent());
            viewRenderer.render(event.getContent());
        }
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setEventPublisher(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void setViewRenderer(Renderer viewRenderer) {
        this.viewRenderer = viewRenderer;
    }
}
