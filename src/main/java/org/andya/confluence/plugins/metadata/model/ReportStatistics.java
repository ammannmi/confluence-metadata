/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import org.andya.confluence.utils.MathUtils;

/**
 * This class represents statistics gathered on behalf of a report.
 */
public class ReportStatistics {
	private final boolean[] shouldTotal;
	private final ColumnStatistics[] columnStatistics;

	public ReportStatistics(int columnCount) {
		shouldTotal = new boolean[columnCount];
		columnStatistics = new ColumnStatistics[columnCount];
	}

	public int getColumnCount() {
		return shouldTotal.length;
	}

	public boolean shouldTotal(int column) {
		return shouldTotal[column];
	}

	public void setShouldTotal(int column, boolean shouldTotal) {
		this.shouldTotal[column] = shouldTotal;
	}

	public String getTotalString(int column) {
		if (!shouldTotal(column))
			return "";
		ColumnStatistics statistics = getColumnStatistics(column);
		Number total = statistics.getTotal();
		return MathUtils.toString(total);
	}

	public ColumnStatistics getColumnStatistics(int column) {
		ColumnStatistics statistics = columnStatistics[column];
		if (statistics == null) {
			statistics = new ColumnStatistics();
			columnStatistics[column] = statistics;
		}
		return statistics;
	}

	public void recordValue(int column, String value) {
		if (!shouldTotal(column) || value == null)
			return;
		ColumnStatistics statistics = getColumnStatistics(column);
		statistics.recordValue(value);
	}
}
