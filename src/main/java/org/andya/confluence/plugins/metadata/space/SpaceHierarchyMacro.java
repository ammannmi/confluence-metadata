/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.andya.confluence.utils.MacroUtils;

import java.util.Map;

/**
 * This macro shows the space hierarchy relative to the current space.
 */
public class SpaceHierarchyMacro extends AbstractSpaceMetadataMacro {
	public SpaceHierarchyMacro() {
		super("space-hierarchy");
	}

	public boolean isInline() {
		return true;
	}

	public boolean hasBody() {
		return false;
	}

	public RenderMode getBodyRenderMode() {
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
		try {
			User user = getUser();
			SpaceMap spaceMap = SpaceMap.get(user, getContentService());
			Space currentSpace = getSpace(renderContext);
			if(parameters.containsKey(SPACE_PARAMETER))
				currentSpace = getSpaceManager().getSpace((String)parameters.get(SPACE_PARAMETER));
			if(currentSpace == null)
				return MacroUtils.showRenderedExceptionString(parameters, "Wrong parameters", "Cannot find space");
			SpaceHierarchy root = currentSpace != null ? spaceMap.getSpaceHierarchy(currentSpace) : spaceMap.getRootHierarchy();
			StringBuffer html = new StringBuffer();
			html.append("<table border='0'>\n");
			appendSpaceInfo(html, renderContext, root, 0);
			html.append("</table>\n");
			return html.toString();
		} catch (Exception e) {
			return MacroUtils.showRenderedExceptionString(parameters, "Unable to render macro", e);
		}
	}

	private void appendSpaceInfo(StringBuffer html, RenderContext renderContext, SpaceHierarchy hierarchy, int depth)
			throws MacroException {
		Space space = hierarchy.getSpace();
		if (space != null) {
			html.append("<tr><td>");
			for (int i=0; i < depth * 4; i++)
				html.append("&nbsp;");
			
			html.append("<img src='" + renderContext.getImagePath() + "/icons/browse_space.gif' width='16' height='16' />");
			html.append("&nbsp;<a href='");
			html.append(renderContext.getSiteRoot() + space.getUrlPath());
			html.append("'>");
			html.append(space.getName());
			html.append("</a>");
			html.append("</td></tr>\n");
		}
		for (SpaceHierarchy child : hierarchy.getChildren()) {
			appendSpaceInfo(html, renderContext, child, depth + 1);
		}
	}
}
