/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import junit.framework.TestCase;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.mock.MockContentEntityObject;
import org.andya.confluence.mock.MockContentService;
import com.atlassian.core.util.DateUtils;

/**
 * The base class of all metadata test cases
 */
public abstract class MetadataTestCase extends TestCase {
	protected static List<MetadataContent> createMockContentMetadata() {
		List<MetadataContent> content = new ArrayList<MetadataContent>();
		Date today = new Date();
		Date tomorrow = DateUtils.tomorrow();
		Date yesterday = DateUtils.yesterday();
		content.add(createMockContent("First New Feature", "Not started", today, new Integer(20)));
		content.add(createMockContent("Second New Feature", "Complete", tomorrow, new Integer(0)));
		content.add(createMockContent("Third New Feature", "In progress", yesterday, new Integer(12)));
		content.add(createMockContent("Fourth New Feature", "", yesterday, new Integer(2)));
		content.add(createMockContent("Fifth New Feature", "In progress", today, "?"));
		return content;
	}

	protected static MetadataContent createMockContent(String name, String status, Date dueDate, Object daysRemaining) {
		MetadataContent content = new MetadataContent(MockContentService.get(), new MockContentEntityObject(name));
		content.putMetadataValue(new MetadataValue("Title", name));
		content.putMetadataValue(new MetadataValue("Status", status));
		content.putMetadataValue(new MetadataValue("Due Date", dueDate, dueDate.toString()));
		content.putMetadataValue(new MetadataValue("Days Remaining", daysRemaining, daysRemaining.toString()));
		return content;
	}
}
