/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page.source;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
import org.andya.confluence.utils.page.PageSearchContext;

public class LabelSource implements PageSource {

	private final String labelName;

	private final LabelManager labelManager;

	private final String spaceKey;

	public LabelSource(String labelName, PageSearchContext context) {
		this.labelName = labelName;
		this.labelManager = context.getLabelManager();
		this.spaceKey = context.getSpaceKey();
	}

	@SuppressWarnings("unchecked")
	public List<Page> getPages(PageSearchContext context) throws MacroException {
		ParsedLabelName ref = LabelParser.parse(labelName);
		List<Page> pages = new ArrayList<Page>();
		if (ref != null) {
			Label label = labelManager.getLabel(ref);
			if (label != null) {
				List entities;
				if (TextUtils.stringSet(spaceKey)) {
					entities = labelManager.getCurrentContentForLabelAndSpace(label, spaceKey);
				} else {
					entities = labelManager.getCurrentContentForLabel(label);
				}
				for(Object entity : entities) {
					if(entity instanceof Page && !((Page)entity).isDeleted())
						pages.add((Page)entity);
				}
			}
		}
		return pages;
	}
}
