/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.*;

import org.andya.confluence.utils.parse.*;

/**
 * Filters an existing list of pages to only those that match the label expression.
 * 
 * This can be used instead of the LabelPageSource in combination with other lists of
 * pages - it removes the need to fetch large numbers of pages and throw lots away.
 * 
 * @author kelsey
 *
 */
public class LabelFilterPageSource extends AbstractLabelPageSource {

	private final PageSource child;


	public LabelFilterPageSource(PageSource child) {
		this.child = child;
	}

	public List<Page> getPages(PageSearchContext context) throws MacroException {

		final List<Page> entities = child.getPages(context);
		final List<Page> filtered = new LinkedList<Page>();
		final Expression expression = getExpression(context);

		//if (true) throw new MacroException("entities:   " + entities);

		for (Page page : entities) {
			LabelInterpreter interpreter = new LabelInterpreter(page);
			expression.accept(interpreter);
			if(interpreter.include) {
				filtered.add(page);
			}
		}

		return filtered;
	}

	static final class LabelInterpreter implements ExpressionVisitor {

		private boolean include;

		private final Set<String> labelNames = new HashSet<String>();

		LabelInterpreter(Page p) {
			List<Label> labels = p.getLabels();
			for (Label label : labels) {
				labelNames.add(label.getName());
			}
			//if (true) throw new RuntimeException("LabelNames:   " + labelNames);
		}

		public void visit(OrExpression or) {
			or.getLeft().accept(this);
			boolean left = include;
			or.getRight().accept(this);
			boolean right = include;
			this.include = left || right;
		}

		public void visit(AndExpression and) {
			and.getLeft().accept(this);
			boolean left = include;
			and.getRight().accept(this);
			boolean right = include;
			this.include = left && right;
		}

		public void visit(LabelExpression label) {
			this.include = labelNames.contains(label.getLabel());
		}
	}
}
