/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import org.andya.confluence.plugins.metadata.MetadataTestCase;
import org.andya.confluence.utils.Duration;
import org.junit.Test;

/**
 * Unit tests for {@link ColumnStatistics}
 */
public class TestColumnStatistics extends MetadataTestCase {
	@Test
	public void testGetTotal() throws Exception {
		// Verify addition of durations
		ColumnStatistics stats = new ColumnStatistics();
		stats.recordValue("1w");
		stats.recordValue("");
		stats.recordValue("3w");
		stats.recordValue("2d");
		assertEquals(Duration.parseDuration("4w 2d"), stats.getTotal());

		// Verify addition of floats
		stats = new ColumnStatistics();
		stats.recordValue("10.1");
		stats.recordValue("");
		stats.recordValue("12");
		assertEquals(new Double(22.1), stats.getTotal());

		// Verify addition of integers
		stats = new ColumnStatistics();
		stats.recordValue("10");
		stats.recordValue("");
		stats.recordValue("12");
		assertEquals(new Integer(22), stats.getTotal());
	}
}
