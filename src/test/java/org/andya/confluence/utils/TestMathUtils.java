/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;

/**
 * Unit tests for {@link MathUtils}.
 */
public class TestMathUtils extends TestCase {
	private static final int TEST_INT = 120;
	private static final int TEST_NEGATIVE_INT = -999;
	private static final double TEST_DOUBLE = 222.22;

	public void testParseNumber() throws Exception {
		Number number = MathUtils.parseNumber("120");
		assertEquals(TEST_INT + " should be parsed as an integer",
		    new Integer(TEST_INT), number);
		number = MathUtils.parseNumber("-999");
		assertEquals(TEST_NEGATIVE_INT + " should be parsed as an integer",
		    new Integer(TEST_NEGATIVE_INT), number);
		number = MathUtils.parseNumber("222.22");
		assertEquals(TEST_DOUBLE + " should be parsed as a double",
		    new Double(TEST_DOUBLE), number);
		number = MathUtils.parseNumber("5d 4h");
		assertEquals("5d 4h should be parsed as a duration",
		    Duration.parseDuration("5d 4h"), number);
	}

	private List<Number> getTestIntegers() {
		return Arrays.asList(new Number[] {
			new Integer(10), new Integer(21), new Integer(-10)
		});
	}

	private List<Number> getTestNumbers() {
		return Arrays.asList(new Number[] {
			new Double(10.5), new Double(20.5), new Integer(-10)
		});
	}

	private List<Number> getTestDurations() {
		return Arrays.asList(new Number[] {
			Duration.parseDuration("1w"), Duration.parseDuration("3w"), Duration.parseDuration("2d")
		});
	}

	public void testSum() throws Exception {
		List<Number> testIntegers = getTestIntegers();
		assertEquals("Sum should be 21",
		    new Integer(21), MathUtils.sum(testIntegers));
		List<Number> testNumbers = getTestNumbers();
		assertEquals("Sum should be 21",
		    new Double(21), MathUtils.sum(testNumbers));
		List<Number> testDurations = getTestDurations();
		assertEquals("Sum should be '4w 2d'",
		    Duration.parseDuration("4w 2d"), MathUtils.sum(testDurations));
	}

	public void testAverage() throws Exception {
		List<Number> testIntegers = getTestIntegers();
		assertEquals("Average should be 7.0",
		    new Double(7.0), MathUtils.average(testIntegers));
		List<Number> testNumbers = getTestNumbers();
		assertEquals("Average should be 7.0",
		    new Double(7.0), MathUtils.average(testNumbers));
		List<Number> testDurations = getTestDurations();
		assertEquals("Average should be '10d'",
		    Duration.parseDuration("10d"), MathUtils.average(testDurations));
	}

	public void testMin() throws Exception {
		List<Number> testIntegers = getTestIntegers();
		assertEquals("Min should be -10",
		    new Integer(-10), MathUtils.min(testIntegers));
		List<Number> testNumbers = getTestNumbers();
		assertEquals("Min should be -10",
		    new Double(-10), MathUtils.min(testNumbers));
		List<Number> testDurations = getTestDurations();
		assertEquals("Min should be '2d'",
		    Duration.parseDuration("2d"), MathUtils.min(testDurations));
	}

	public void testMax() throws Exception {
		List<Number> testIntegers = getTestIntegers();
		assertEquals("Max should be 21",
		    new Integer(21), MathUtils.max(testIntegers));
		List<Number> testNumbers = getTestNumbers();
		assertEquals("Max should be 20.5",
		    new Double(20.5), MathUtils.max(testNumbers));
		List<Number> testDurations = getTestDurations();
		assertEquals("Max should be '3w'",
		    Duration.parseDuration("3w"), MathUtils.max(testDurations));
	}
}
