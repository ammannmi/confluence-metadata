/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page;

import java.util.LinkedList;
import java.util.List;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.labels.*;
import com.atlassian.confluence.pages.Page;
import com.opensymphony.util.TextUtils;
import org.andya.confluence.utils.parse.ExpressionVisitor;
import org.andya.confluence.utils.parse.OrExpression;
import org.andya.confluence.utils.parse.AndExpression;
import org.andya.confluence.utils.parse.LabelExpression;

/**
 * Walks a label expression tree collecting the appropriate pages
 * 
 * @author kelsey
 *
 */
class LabelExpressionVisitor implements ExpressionVisitor {

	/** The pages we have collected */
	private List<Page> pages;

	private final PageSearchContext context;

	public LabelExpressionVisitor(PageSearchContext context) {
		this.context = context;
	}

	/**
	 * Get the pages that have matched this expression. Returns null if this
	 * visitor has not visited an expression yet.
	 * 
	 * @return the pages that have matched this expression
	 */
	public List<Page> getPages() {
		return pages;
	}

	public void visit(OrExpression or) {
		or.getLeft().accept(this);
		List<Page> pagesLeft = this.pages;
		or.getRight().accept(this);
		List<Page> pagesRight = this.pages;

		// add them all!
		pagesLeft.addAll(pagesRight);
		this.pages = pagesLeft;
	}

	public void visit(AndExpression and) {
		and.getLeft().accept(this);
		List<Page> pagesLeft = this.pages;
		and.getRight().accept(this);
		List<Page> pagesRight = this.pages;

		// only keep if in both
		pagesLeft.retainAll(pagesRight);
		this.pages = pagesLeft;
	}

	@SuppressWarnings("unchecked")
	public void visit(LabelExpression labelExpression) {
		this.pages = new LinkedList<Page>();
		final LabelManager labelManager = context.getLabelManager();
		final String spaceKey = context.getSpaceKey();
		String labelName = labelExpression.getLabel();

		ParsedLabelName ref = LabelParser.parse(labelName);
		if (ref != null)
		{
			Label label = labelManager.getLabel(ref);
			if (label != null)
			{
				List entities;
				if (TextUtils.stringSet(spaceKey))
					entities = labelManager.getCurrentContentForLabelAndSpace(label, spaceKey);
				else
					entities = labelManager.getCurrentContentForLabel(label);
				for (Object entity : entities) {
					if(entity instanceof Page && !((Page)entity).isDeleted())
						pages.add((Page)entity);					
				}
			}
		}
	}


}