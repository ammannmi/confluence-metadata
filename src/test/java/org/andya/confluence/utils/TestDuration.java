/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils;

import junit.framework.TestCase;

/**
 * Unit tests for {@link Duration}.
 */
public class TestDuration extends TestCase {
	private static final int SECOND = 1;
	private static final int MINUTE = 60 * SECOND;
	private static final int HOUR = 60 * MINUTE;
	private static final int DAY = 24 * HOUR;
	private static final int WEEK = 7 * DAY;

	private static final DurationExample[] durationExamples = new DurationExample[] {
		new DurationExample("4w", 4 * WEEK),
		new DurationExample("28d", 28 * DAY),
		new DurationExample("1h 30m", HOUR + 30 * MINUTE),
	};

	public void testIsDuration() {
		for (int i=0; i < durationExamples.length; i++) {
			DurationExample example = durationExamples[i];
			assertTrue(Duration.isDuration(example.getDurationString()));
		}
		assertFalse(Duration.isDuration("100"));
	}

	public void testParseDuration() {
		for (int i=0; i < durationExamples.length; i++) {
			DurationExample example = durationExamples[i];
			assertEquals(new Duration(example.getDuration()), Duration.parseDuration(example.getDurationString()));
		}
		assertNull(Duration.parseDuration("100"));
	}

	private static class DurationExample {
		private final String durationString;
		private final long duration;

		public DurationExample(String durationString, long duration) {
			this.durationString = durationString;
			this.duration = duration;
		}

		public String getDurationString() {
			return durationString;
		}

		public long getDuration() {
			return duration;
		}
	}
}
