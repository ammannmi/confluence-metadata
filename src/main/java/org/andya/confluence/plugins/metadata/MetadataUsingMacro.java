/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;

import java.util.*;

import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.utils.page.PageSearchContext;
import org.andya.confluence.utils.page.PageSource;
import org.andya.confluence.utils.page.PageLocator;
import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.utils.MathUtils;
import org.andya.confluence.utils.ContentService;

/**
 * The abstract base class of any macro that uses metadata from other pages.
 */
public abstract class MetadataUsingMacro extends AbstractMetadataMacro {
	private static final List<String> VALID_TYPES = Arrays.asList(new String[] {
		Page.CONTENT_TYPE, BlogPost.CONTENT_TYPE
	});

	protected MetadataUsingMacro(String macroName) {
		super(macroName);
	}

	public boolean isInline() {
		return false;
	}

	public boolean hasBody() {
		return false;
	}

	public RenderMode getBodyRenderMode() {
		return RenderMode.ALL;
	}

	public boolean suppressMacroRenderingDuringWysiwyg() {
		return true;
	}

	/**
	 * Returns a list of matching pages based on the parameters to the macro.
	 * @param parameters The macro's parameters.
	 * @param renderContext The render context.
	 * @param ordered If true then the results should be ordered according to the sort parameters.
	 * @return A list of matching pages
	 * @throws MacroException Thrown if any of the parameters are inappropriate.
	 */
	@SuppressWarnings("unchecked")
	public List<MetadataContent> getMatchingPages(Map parameters, ContentService contentService, RenderContext renderContext,
															 boolean ordered) throws MacroException {
		PermissionManager permissionManager = getPermissionManager();

		final PageSearchContext context = new PageSearchContext(parameters, getLabelManager(), renderContext, getPageManager(),
				permissionManager);
		String type = MacroUtils.getStringParameter(parameters, TYPE_PARAMETER, null);

		PageSource source = new PageLocator();
		List<Page> pages = source.getPages(context);

		//		remove duplicates and filter by permission and type
		pages = removeDuplicates(pages);
		pages = permissionManager.getPermittedEntities(getUser(), Permission.VIEW, pages);
		pages = filterByContentType(pages, type);
		List<ConfluenceEntityObject> entities = new ArrayList<ConfluenceEntityObject>();
		for(Page page : pages) {
			entities.add(page);
		}
		return getSortedContents(parameters, entities, ordered ? ContentService.TITLE_KEY : null);
	}

	private List<Page> filterByContentType(List<Page> entities, String type) throws MacroException {
		List<String> contentTypes = new ArrayList<String>();
		if (TextUtils.stringSet(type))
		{
			StringTokenizer st = new StringTokenizer(type, ",");
			while (st.hasMoreTokens()) {
				String contentType = st.nextToken();

				// Only add the content type if it's supported
				if (VALID_TYPES.contains(contentType))
					contentTypes.add(contentType);
				else
					throw new MacroException(contentType + " is not a supported content type.");
			}
		}

		// Only filter the content when we have had a type specified
		if (contentTypes.size() > 0)
			return filterByContentType(entities, contentTypes);
		return entities;
	}

	private List<Page> removeDuplicates(List<Page> contents) {
		Map<String, String> contentMap = new HashMap<String, String>();

		List<Page> filteredList = new ArrayList<Page>();
		for (Page o : contents) {
			if (!contentMap.containsKey(o.getIdAsString())) {
				contentMap.put(o.getIdAsString(), o.getIdAsString());
				filteredList.add(o);
			}
		}
		return filteredList;
	}

	/**
	 * @param original
	 * @param types list of types (the ones you get from calling <ContentEntityObject>.getType())
	 * @return list of ContentEntityObjects whose type matches those specified
	 */
	private List<Page> filterByContentType(List<Page> original, List<String> types)	{
		List<Page> result = new ArrayList<Page>();
		for (Page page : original)
		{
			if (types.contains(page.getType()))
				result.add(page);
		}
		return result;
	}

	protected String renderCalculation(String functionName, List<Number> values) throws MacroException {
		if (values.isEmpty()) {
			return "";
		} else {
			Number result = MathUtils.doCalculation(functionName, values);
			return MathUtils.toString(result);
		}
	}
}
