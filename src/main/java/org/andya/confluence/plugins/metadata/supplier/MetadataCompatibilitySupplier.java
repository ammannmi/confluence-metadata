package org.andya.confluence.plugins.metadata.supplier;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResult;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.UserManager;
import org.andya.confluence.plugins.metadata.MetadataUtils;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.randombits.supplier.core.SupplierContext;
import org.randombits.supplier.core.annotate.AnnotatedSupplier;
import org.randombits.supplier.core.annotate.KeyContext;
import org.randombits.supplier.core.annotate.KeyParam;
import org.randombits.supplier.core.annotate.SupplierKey;
import org.randombits.supplier.core.annotate.SupplierPrefix;
import org.randombits.supplier.core.annotate.SupportedTypes;

/* Metadata Supplier for Reporting Plugin versions below 6.0, left for backward compatibility. */
@SupplierPrefix("metadata")
@SupportedTypes({ContentEntityObject.class, LuceneSearchResult.class})
public class MetadataCompatibilitySupplier extends AnnotatedSupplier {
	protected Log log = LogFactory.getLog(this.getClass());
	private ContentService contentService;
	private  ContentPropertyManager contentPropertyManager;
	private  FormatSettingsManager formatSettingsManager;
	private  SpaceManager spaceManager;
	private  UserManager userManager;
	private  PageManager pageManager;



	/**
	 * Returns the metadata value by key.
	 * 
	 * @param context
	 *            The supplier context.
	 * @param key
	 *            The metadata key. Leave blank to return all stored metadata.
	 * @return if context contains a CEO (page) or a LuceneSearchResult :  The value of the given metadata key.
     *          else it returns the context>value object whatever it is.
	 */
	@SupplierKey("{key}")
	public Object getContent(@KeyContext SupplierContext context, @KeyParam("key") String key) {
		Object value = context.getValue();
		ContentEntityObject page;
		// not sure what object is returned via "context.getValue()" here
		// hence check which one
		if (value instanceof ContentEntityObject) {
			page = (ContentEntityObject) value;
		} else if ( value instanceof LuceneSearchResult) {
			LuceneSearchResult currentContent = (LuceneSearchResult) value;
			page = pageManager.getPage(currentContent.getSpaceKey(),
					currentContent.getDisplayTitle());
		} else {
            return context.getValue();
        }
		if (StringUtils.isBlank(key)) {
			// we keep the old implementation here in order to break no other code
			return MetadataUtils.getAllMetadata(getContentService(), page).entrySet();
		}
		MetadataValue metadataValue = MetadataUtils.getMetadataValue(getContentService(), page,
				key, null);

		String stringValue = metadataValue != null ? metadataValue.getWikiSnippet() : null;
		if (StringUtils.isNotBlank(stringValue)) {
			DateTimeFormatter dateParser = ISODateTimeFormat.dateParser();
			try {
				// if the value is a date return it as a date
				return dateParser.parseDateTime(stringValue).toDate();
			} catch (Exception e) {
				// ignore and keep the old value
			}
		}
		return stringValue;
	}

	private ContentService getContentService() {
		if (contentService == null) {
			contentService = new ContentService(contentPropertyManager, spaceManager,
					userManager, formatSettingsManager);
		}
		return contentService;
	}


    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
        this.contentPropertyManager = contentPropertyManager;
    }

    public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager) {
        this.formatSettingsManager = formatSettingsManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }
}