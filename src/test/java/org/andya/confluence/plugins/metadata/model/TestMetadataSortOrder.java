/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import java.util.List;
import java.util.Comparator;
import java.util.Collections;

import org.andya.confluence.plugins.metadata.MetadataTestCase;
import org.andya.confluence.utils.ContentService;
import org.andya.confluence.utils.MacroConstants;
import org.junit.Test;

/**
 * Unit tests for {@link MetadataSortOrder}.
 */
public class TestMetadataSortOrder extends MetadataTestCase {
	@Test
	public void testComparator() throws Exception {
		// Sorting columns by status should use numeric sort
		List<MetadataContent> contents = createMockContentMetadata();
		MetadataSortOrder order = new MetadataSortOrder("Status", true, MacroConstants.SORT_DEFAULT);
		Comparator<MetadataContent> comparator = MetadataSortOrder.getComparator(new MetadataSortOrder[] { order }, ContentService.TITLE_KEY);
		Collections.sort(contents, comparator);
		assertEquals("", ((MetadataContent)contents.get(0)).getMetadataWikiSnippet("Status"));
		assertEquals("Complete", ((MetadataContent)contents.get(1)).getMetadataWikiSnippet("Status"));
		assertEquals("In progress", ((MetadataContent)contents.get(2)).getMetadataWikiSnippet("Status"));
		assertEquals("In progress", ((MetadataContent)contents.get(3)).getMetadataWikiSnippet("Status"));
		assertEquals("Not started", ((MetadataContent)contents.get(4)).getMetadataWikiSnippet("Status"));

		// Sorting columns by numbers should use numeric sort
		order = new MetadataSortOrder("Days Remaining", false, MacroConstants.SORT_NUMBER);
		comparator = MetadataSortOrder.getComparator(new MetadataSortOrder[] { order }, ContentService.TITLE_KEY);
		Collections.sort(contents, comparator);
		assertEquals("?", ((MetadataContent)contents.get(0)).getMetadataWikiSnippet("Days Remaining"));
		assertEquals(new Integer(20), ((MetadataContent)contents.get(1)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(12), ((MetadataContent)contents.get(2)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(2), ((MetadataContent)contents.get(3)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(0), ((MetadataContent)contents.get(4)).getMetadataNumber("Days Remaining"));

		// Reverse sorting columns by numbers should return the correct results
		order = new MetadataSortOrder("Days Remaining", true, MacroConstants.SORT_NUMBER);
		comparator = MetadataSortOrder.getComparator(new MetadataSortOrder[] { order }, ContentService.TITLE_KEY);
		Collections.sort(contents, comparator);
		assertEquals(new Integer(0), ((MetadataContent)contents.get(0)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(2), ((MetadataContent)contents.get(1)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(12), ((MetadataContent)contents.get(2)).getMetadataNumber("Days Remaining"));
		assertEquals(new Integer(20), ((MetadataContent)contents.get(3)).getMetadataNumber("Days Remaining"));
		assertEquals("?", ((MetadataContent)contents.get(4)).getMetadataWikiSnippet("Days Remaining"));

		// Sorting columns by date should work correctly
		order = new MetadataSortOrder("Due Date", false, MacroConstants.SORT_DATE);
		comparator = MetadataSortOrder.getComparator(new MetadataSortOrder[] { order }, ContentService.TITLE_KEY);
		Collections.sort(contents, comparator);
		assertEquals("Second New Feature", ((MetadataContent)contents.get(0)).getMetadataWikiSnippet("Title"));
		assertEquals("Fifth New Feature", ((MetadataContent)contents.get(1)).getMetadataWikiSnippet("Title"));
		assertEquals("First New Feature", ((MetadataContent)contents.get(2)).getMetadataWikiSnippet("Title"));
		assertEquals("Fourth New Feature", ((MetadataContent)contents.get(3)).getMetadataWikiSnippet("Title"));
		assertEquals("Third New Feature", ((MetadataContent)contents.get(4)).getMetadataWikiSnippet("Title"));
	}

	public void testGetMetadataSortOrder() throws Exception {
		MetadataSortOrder[] sortOrders = MetadataSortOrder.getMetadataSortOrders("Title,Days Remaining desc");
		assertEquals("Should have two sort orders", 2, sortOrders.length);
		assertEquals("First sort order should have correct name",
				"Title", sortOrders[0].getMetadataName());
		assertTrue("First sort order should be ascending",
				sortOrders[0].isAscending());
		assertEquals("Second sort order should have correct name",
				"Days Remaining", sortOrders[1].getMetadataName());
		assertTrue("Second sort order should be descending",
				!sortOrders[1].isAscending());

		sortOrders = MetadataSortOrder.getMetadataSortOrders("Title,Days Remaining");
		assertEquals("Should have two sort orders", 2, sortOrders.length);
		assertEquals("First sort order should have correct name",
				"Title", sortOrders[0].getMetadataName());
		assertTrue("First sort order should be ascending",
				sortOrders[0].isAscending());
		assertEquals("Second sort order should have correct name",
				"Days Remaining", sortOrders[1].getMetadataName());
		assertTrue("Second sort order should be ascending",
				sortOrders[1].isAscending());

		sortOrders = MetadataSortOrder.getMetadataSortOrders("Page desc");
		assertEquals("Should have one sort order", 1, sortOrders.length);
		MetadataSortOrder sortOrder = sortOrders[0];
		assertEquals("First sort order should have correct name",
				"Page", sortOrder.getMetadataName());
		assertTrue("First sort order should be descending",
				!sortOrder.isAscending());
	}
}
