/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.renderers;

import com.atlassian.confluence.content.render.xhtml.LinkRenderer;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.AbstractPage;

import java.util.List;
import java.util.Arrays;

import org.andya.confluence.utils.ContentUtils;
import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.plugins.metadata.model.ReportStatistics;
import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.model.MetadataValue;

/**
 * This renderer displays a list of metadata contents in a table.
 */
public class MetadataTableRenderer implements MetadataRenderer {
	private final SubRenderer subRenderer;

    private final MetadataSingleValueRenderer metadataValueRenderer;

    public MetadataTableRenderer(SubRenderer subRenderer, LinkRenderer linkRenderer) {
        this.subRenderer = subRenderer;
        this.metadataValueRenderer = new MetadataSingleValueRenderer(linkRenderer);
    }

    public SubRenderer getSubRenderer() {
		return subRenderer;
	}

	public String render(RenderContext renderContext, List<MetadataContent> contents, int maxResults,
											 String[] columns, String commaDelimitedTotals, String linkColumn) throws MacroException {
		int totalResults = contents.size();
		boolean truncated = false;
		if (totalResults > maxResults) {
			contents = contents.subList(0, maxResults);
			truncated = true;
		}
		ReportStatistics statistics = new ReportStatistics(columns.length);
		boolean showTotals = commaDelimitedTotals.length() > 0;
		if (showTotals) {
			List<String> totalColumns = Arrays.asList(commaDelimitedTotals.split(","));
			for (int i=0; i < columns.length; i++) {
				String column = columns[i];
				statistics.setShouldTotal(i, totalColumns.contains(column));
			}
		}
		StringBuffer html = new StringBuffer();
		html.append("<table class='confluenceTable'>\n");
		writeHeadings(html, columns);
		for (MetadataContent content : contents) {
			writeTableRow(html, content, renderContext, columns, linkColumn, statistics);
		}
		if (truncated) {
			html.append("<tr><td colspan=" + columns.length + " class='confluenceTd'>");
			html.append("First " + maxResults + " results of " + totalResults + "...");
			html.append("</td></tr>\n");
		}
		if (showTotals)
			writeTotals(html, statistics);
		html.append("</table>\n");
		return html.toString();
	}

	private void writeHeadings(StringBuffer html, String[] columns) {
		html.append("<tr>");
		for (int i=0; i < columns.length; i++) {
			String column = columns[i];
			html.append("<th class='confluenceTh'>");
			html.append(column);
			html.append("</th>");
		}
		html.append("</tr>\n");
	}

    private void writeTableRow(StringBuffer html, MetadataContent content, RenderContext renderContext,
                               String[] columns, String linkColumn, ReportStatistics statistics) {
        SubRenderer subRenderer = getSubRenderer();
        html.append("<tr>");
        ConfluenceEntityObject ceo = content.getEntity();
        AbstractPage page = ceo instanceof AbstractPage ? (AbstractPage) ceo : null;
        for (int i = 0; i < columns.length; i++) {
            String column = columns[i];
            MetadataValue value = content.getMetadataValue(column);
            html.append("<td class='confluenceTd'>");
            String valueString = metadataValueRenderer.renderSingleValue(html, renderContext, linkColumn, subRenderer,
                    ceo, page, column, value);
            html.append("</td>");
            statistics.recordValue(i, valueString);
        }
        html.append("</tr>\n");
    }

    private void writeTotals(StringBuffer html, ReportStatistics statistics) {
		html.append("<tr>");
		int columnCount = statistics.getColumnCount();
		for (int i=0; i < columnCount; i++) {
			html.append("<td class='confluenceTd'>");
			if (statistics.shouldTotal(i)) {
				html.append("<b>");
				html.append(statistics.getTotalString(i));
				html.append("</b>");
			} else if (i == 0) {
				html.append("<b>Totals</b>");
			} else {
				html.append("&nbsp;");
			}
			html.append("</td>");
		}
		html.append("</tr>\n");
	}
}
