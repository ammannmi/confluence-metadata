/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import org.andya.confluence.utils.MacroUtils;

import java.util.Map;

/**
 * This macro allows the user to provide a list of metadata values and to have them
 * rendered onto the page.
 */
public class MetadataListMacro extends AbstractMetadataMacro {
	public MetadataListMacro() {
		super("metadata-list");
	}

	public boolean isInline() {
		return false;
	}

	public boolean hasBody() {
		return true;
	}

	public RenderMode getBodyRenderMode() {
		return null;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
		try {
			boolean hidden = MacroUtils.getBooleanParameter(parameters, HIDDEN_PARAMETER);
			boolean clear = MacroUtils.getBooleanParameter(parameters, CLEAR_PARAMETER);
			boolean horizontal = MacroUtils.getBooleanParameter(parameters, ORIENTATION_PARAMETER, HORIZONTAL_KEY, false);
			Page page = getPage(renderContext);
			if (page != null && clear)
				MetadataUtils.clearMetadataNames(getContentPropertyManager(), page);
			return addMetadataList(page, body, hidden, horizontal, renderContext.getOutputType());
		} catch (Exception e) {
			return showUnrenderedExceptionString(parameters, e);
		}
	}
}
