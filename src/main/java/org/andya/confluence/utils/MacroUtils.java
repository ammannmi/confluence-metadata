/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils;

import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderUtils;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.RenderContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.renderer.PageContext;

import java.util.Map;
import java.io.StringWriter;
import java.io.PrintWriter;

/**
 * Useful utilitis for building macros.
 */
public class MacroUtils {
	private static final String DEBUG_KEY = "debug";

	@SuppressWarnings("unchecked")
	public static String getStringParameter(Map parameters, String name, boolean required) throws MacroException {
		String value = (String)parameters.get(name);
		if (value == null && required)
			throw new MacroException("Required parameter \"" + name + "\" not provided");
		return value;
	}

    @SuppressWarnings("unchecked")
	public static String getStringParameter(Map parameters, String name, String defaultValue) throws MacroException {
		String value = getStringParameter(parameters, name, false);
		return value != null ? value : defaultValue;
	}

	@SuppressWarnings("unchecked")
	public static Integer getIntegerParameter(Map parameters, String name) throws MacroException {
		String value = getStringParameter(parameters, name, false);
		if (value == null)
			return null;
		try {
			return Integer.valueOf(value);
		} catch (NumberFormatException e) {
			throw new MacroException("Non-integer value for parameter \"" + name + "\": " + value);
		}
	}

	@SuppressWarnings("unchecked")
	public static int getIntParameter(Map parameters, String name, int defaultValue) throws MacroException {
		Integer result = getIntegerParameter(parameters, name);
		return result != null ? result.intValue() : defaultValue;
	}

	@SuppressWarnings("unchecked")
	public static boolean getBooleanParameter(Map parameters, String name) throws MacroException {
		return getBooleanParameter(parameters, name, false);
	}

	@SuppressWarnings("unchecked")
	public static boolean getBooleanParameter(Map parameters, String name, boolean defaultValue) throws MacroException {
		return getBooleanParameter(parameters, name, "true", defaultValue);
	}

	@SuppressWarnings("unchecked")
	public static boolean getBooleanParameter(Map parameters, String name, String trueKey, boolean defaultValue) throws MacroException {
		String value = getStringParameter(parameters, name, null);
		return value != null ? trueKey.equals(value) : defaultValue;
	}

	@SuppressWarnings("unchecked")
	public static boolean isDebugMode(Map parameters) {
		try {
			return getBooleanParameter(parameters, DEBUG_KEY, false);
		} catch (MacroException e) {
			return false;
		}
	}

	public static String getExceptionString(Throwable e) {
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}

	public static String getExceptionMessage(Throwable e) {
		String message = e.getMessage();
		if (message == null)
			message = e.toString();
		return message;
	}

	/** Renders an exception to be displayed from a macro. */
	@SuppressWarnings("unchecked")
	public static String showRenderedExceptionString(Map parameters, String title, Throwable e) {
        e.printStackTrace();
		if (isDebugMode(parameters))
			return "<div class=\"error\"><h3>" + title + "</h3>\n<pre>" + getExceptionString(e) + "</pre></div>\n";
		else
			return RenderUtils.blockError(title + ":", getExceptionMessage(e));
	}

	/** Renders an exception to be displayed from a macro. */
	@SuppressWarnings("unchecked")
	public static String showRenderedExceptionString(Map parameters, String title, String message) {
		if (isDebugMode(parameters))
			return "<div class=\"error\"><h3>" + title + "<h3>\n<pre>" + message + "</pre></div>\n";
		else
			return RenderUtils.blockError(title + ":", message);
	}

	/** Returns an unrendered message string for an exception. */
	@SuppressWarnings("unchecked")
	public static String showUnrenderedExceptionString(Map parameters, String title, Throwable e) {
		if (isDebugMode(parameters))
			return "{warning:title=" + title + "}{noformat}" + getExceptionString(e) + "{noformat}{warning}\n";
		else {
			return "{warning:title=" + title + "}" + getExceptionMessage(e) + "{warning}\n";
		}
	}

	/** Returns an unrendered message string for an exception. */
	@SuppressWarnings("unchecked")
	public static String showUnrenderedExceptionString(Map parameters, String title, String message) {
		if (isDebugMode(parameters))
			return "{warning:title=" + title + "}" + message + "{warning}\n";
		else
			return "{warning:title=" + title + "}" + message + "{warning}\n";
	}

	/** Renders a page value for the given render context. */
	public static String renderValue(SubRenderer subRenderer, RenderContext renderContext, String value) {
		return subRenderer.render(value != null ? value : "", renderContext,
				renderContext.getRenderMode().and(RenderMode.suppress(RenderMode.F_FIRST_PARA)));
	}

	/** Renders a content entity object's metadata value. */
	public static String renderValue(SubRenderer subRenderer, RenderContext renderContext,
																	 ConfluenceEntityObject ceo, String value) {
		if (renderContext instanceof PageContext && ceo instanceof ContentEntityObject) {
			PageContext pageContext = new PageContext((ContentEntityObject)ceo, (PageContext)renderContext);
			return renderValue(subRenderer, pageContext, value);
		} else {
			return renderValue(subRenderer, renderContext, value);
		}
	}
}
