package org.andya.confluence.plugins.metadata.renderers;

/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentUtils;
import org.andya.confluence.utils.MacroUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.LinkRenderer;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.SubRenderer;

/**
 * Common class for rendering single meta-data values independent of the
 * context as HTML snippet.
 *
 * Used to avoid duplicate code in {@link MetadataRenderer}, such as {@link MetadataListRenderer} and
 * {@link MetadataTableRenderer}.
 *
 */
public class MetadataSingleValueRenderer {
	private final Log log = LogFactory.getLog(this.getClass());
	private final LinkRenderer linkRenderer;

	public MetadataSingleValueRenderer(LinkRenderer linkRenderer) {
		this.linkRenderer = linkRenderer;
	}

	/**
	 * Renders a single meta-data value as HTML snippet.
	 */
	public String renderSingleValue(StringBuffer html, RenderContext renderContext, String linkColumn,
			SubRenderer subRenderer, ConfluenceEntityObject ceo, AbstractPage page, String column, MetadataValue value) {
		String valueString = "";
		boolean isPageLinkKey = ContentUtils.isPageLinkKey(column);
		boolean pageNotNull = page != null;
		// if this is a link for a page do not us the wiki markup render
		// page titles in confluence > 4 can contain certain characters,
		// such as "@" that have a special meaning in the wiki markup syntax
		if (isPageLinkKey && pageNotNull) {
			try {
				valueString = linkRenderer.render(page, new DefaultConversionContext(renderContext));
			} catch (XhtmlException e) {
				log.warn("Error rendering link for page " + page.getSpace() + ":" + page.getTitle(), e);
			}
		} else {
			// treat the other values as usual
			valueString = MacroUtils.renderValue(subRenderer, renderContext, ceo, MetadataValue.getWikiSnippet(value));
		}
		boolean isLink = column.equalsIgnoreCase(linkColumn);
		if (!isPageLinkKey && isLink && pageNotNull) {
			// wrap a link around standard values for the specified column
			html.append("<a href='");
			String pageURL = ContentUtils.getPageURL(renderContext, page);
			html.append(pageURL);
			html.append("'>");
			html.append(valueString);
			html.append("</a>");
		} else {
			html.append(valueString);
		}
		return valueString;
	}

}