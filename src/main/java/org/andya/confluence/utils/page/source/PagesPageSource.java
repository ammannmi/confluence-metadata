/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page.source;

import java.util.LinkedList;
import java.util.List;

import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import org.andya.confluence.utils.page.PageSearchContext;
import org.andya.confluence.utils.page.IndividualPageLocator;

/**
 * This fetches pages by specifying a specific page either directly or by special 
 * tags @self or @parent.
 * 
 * @author kelsey
 *
 */
public class PagesPageSource implements PageSource {

	private final String pageValue;

	public PagesPageSource(String pageValue) {
		this.pageValue = pageValue;
	}



	public List<Page> getPages(PageSearchContext context) throws MacroException {

		final RenderContext renderContext = context.getRenderContext();
		final String spaceKey = context.getSpaceKey();
		// if there is more than one...
		final String[] pageNames = pageValue.split(",");

		final IndividualPageLocator finder = new IndividualPageLocator(context.getPageManager());

		final List<Page> pages = new LinkedList<Page>();

		// now construct an explicit list of pages
		for (int i = 0; i < pageNames.length; i++) {
			String pageName = pageNames[i];
			pages.add(finder.find(pageName, renderContext, spaceKey));
		}

		return pages;
	}
}
