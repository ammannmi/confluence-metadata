/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.mock;

import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.core.DefaultFormatSettingsManager;

/**
 * Used for testing. It always returns the defaults from {@link com.atlassian.confluence.core.DefaultFormatSettingsManager}
 * and ignore calls to the setters.
 */
public class MockFormatSettingsManager implements FormatSettingsManager
{
		public String getDateFormat()
		{
				return DefaultFormatSettingsManager.DEFAULT_DATE_PATTERN;
		}

		public void setDateFormat(String pattern)
		{
		}

		public String getTimeFormat()
		{
				return DefaultFormatSettingsManager.DEFAULT_TIME_PATTERN;
		}

		public void setTimeFormat(String pattern)
		{
		}

		public String getDateTimeFormat()
		{
				return DefaultFormatSettingsManager.DEFAULT_DATE_TIME_PATTERN;
		}

		public void setDateTimeFormat(String pattern)
		{
		}

		public String getLongNumberFormat()
		{
				return DefaultFormatSettingsManager.DEFAULT_LONG_NUMBER_PATTERN;
		}

		public void setLongNumberFormat(String pattern)
		{
		}

		public String getDecimalNumberFormat()
		{
				return DefaultFormatSettingsManager.DEFAULT_DECIMAL_NUMBER_PATTERN;
		}

		public void setDecimalNumberFormat(String pattern)
		{
		}

    @Override
    public String getEditorBlogPostDate() {
        return null;
    }

    public String getDayFormat() {
			// TODO Auto-generated method stub
			return DefaultFormatSettingsManager.DEFAULT_DATE_PATTERN;
		}

		public void setDayFormat(String arg0) {
			// TODO Auto-generated method stub
			
		}
}
